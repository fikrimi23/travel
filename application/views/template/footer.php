    <footer class="container-fluid text-center footer">
        <div class="col-sm-3">
            <div class="btn btn-link"><span><img class="icon" src="<?php echo asset_url('gambar/line.png') ?>" alt="">@funVac</span></div>
        </div>
        <div class="col-sm-3">
            <div class="btn btn-link"><span><img class="icon" src="<?php echo asset_url('gambar/twitter.png') ?>" alt="">@funVacation</span></div>
        </div>
        <div class="col-sm-3">
            <div class="btn btn-link"><span><img class="icon" src="<?php echo asset_url('gambar/wa.png') ?>" alt="">+6281666777666</span></div>
        </div>
        <div class="col-sm-3">
            <div class="btn btn-link"><span><img class="icon" src="<?php echo asset_url('gambar/instagram.png') ?>" alt="">@funVacation</span></div>
        </div>
    </footer>
</body>
<!-- login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><center>Login</center></h4>
            </div>
            <div class="modal-body">
                <div class='panel panel-info' >
                    <div class='panel-heading'>
                        <div class='panel-title'>Sign In</div>
                    </div>

                    <div style='padding-top:30px' class='panel-body' >
                        <div style='display:none' id='login-alert' class='alert alert-danger col-sm-12'></div>
                        <form id='loginform' method="post" class='form-horizontal' role='form' action="<?php echo site_url('auth/login_auth'); ?>">
                            <div style='margin-bottom: 25px' class='input-group'>
                                <span class='input-group-addon'><i class='fa fa-fw fa-user'></i></span>
                                <input id='login-username' type='text' class='form-control' name='username' value='' placeholder='username or email'>
                            </div>
                            <div style='margin-bottom: 25px' class='input-group'>
                                <span class='input-group-addon'><i class='fa fa-fw fa-key'></i></span>
                                <input id='login-password' type='password' class='form-control' name='password' placeholder='password'>
                            </div>
                            <div style='margin-top:10px' class='form-group'>
                                <!-- Button -->
                                <div class='col-sm-12 controls'>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Login</button>
                                </div>
                            </div>
                            <div class="etc-login-form">
                                <p>new user? <a href="<?php echo site_url('auth/register'); ?>">create new account</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
