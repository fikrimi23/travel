<!DOCTYPE html>
<html lang="en">
<head>
    <title>Max Your Holiday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo asset_url('css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo asset_url('css/bootstrap-material-datetimepicker.css') ?>">
    <link rel="stylesheet" href="<?php echo asset_url('css/public.css') ?>">
    <link rel="stylesheet" href="<?php echo asset_url('css/font-awesome.min.css') ?>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url('js/moment.js') ?>"></script>
    <script src="<?php echo asset_url('js/bootstrap-material-datetimepicker.js') ?>"></script>
    <title></title>
</head>
<body>
    <style>
        .container {
            margin-top: 40px;
        }
    </style>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo site_url('/') ?>">HOME</a></li>
                <li class="active" ><a href="<?php echo site_url('about') ?>">ABOUT</a></li>
                <li><a href="<?php echo site_url('gallery') ?>">GALLERY</a></li>
                <li><a href="<?php echo site_url('faq') ?>">F.A.Q</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == 1): ?>
                    <li><a class="btn btn-link" href="<?php echo site_url('admin/users') ?>"></span>Users</a></li>
                    <li><a class="btn btn-link" href="<?php echo site_url('admin/history') ?>">History</a></li>
                <?php endif ?>

                <?php if (isset($_SESSION['is_login']) && $_SESSION['is_login'] == 1): ?>
                    <li><a class="btn btn-link" href="<?php echo site_url('user/profile') ?>"></span>Profil</a></li>
                    <li><a class="btn btn-link" href="<?php echo site_url('user/history') ?>">Transaction</a></li>
                    <li><a class="btn btn-link" href="#"><?php echo $_SESSION['username'] ?></a></li>
                    <li><a class="btn btn-link" href="<?php echo site_url('auth/logout') ?>">Logout</a></li>
                <?php else: ?>
                    <li><a class="btn btn-link" data-toggle="modal" data-target="#login">Login</a></li>
                <?php endif ?>
            </ul>
        </div>
    </nav>
