<!DOCTYPE html>
<html lang="en">
<head>
    <title>Max Your Holiday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo asset_url('css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo asset_url('css/bootstrap-material-datetimepicker.css') ?>">
    <link rel="stylesheet" href="<?php echo asset_url('css/public.css') ?>">
    <link rel="stylesheet" href="<?php echo asset_url('css/font-awesome.min.css') ?>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url('js/moment.js') ?>"></script>
    <script src="<?php echo asset_url('js/bootstrap-material-datetimepicker.js') ?>"></script>
    <title></title>
</head>
<body>
    <style>
        .container {
            margin-top: 40px;
        }
    </style>
