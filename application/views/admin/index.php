<div class="container">
    <!--ISI-->
    <table class="table table-hover">
        <thead>
            <th>No</th>
            <th>Nama</th>
            <th>Username</th>
            <th>Email</th>
            <th>Action</th>
        </thead>
        <tbody>
            <?php $x = 1; foreach ($users as $user): ?>
                <tr>
                    <td><?php echo $x++ ?></td>
                    <td><?php echo $user->nama ?></td>
                    <td><?php echo $user->username ?></td>
                    <td><?php echo $user->email ?></td>
                    <td>
                        <a href="<?php echo site_url('admin/users/'.$user->id.'/edit') ?>" class="btn btn-info"><i class="fa fa-fw fa-edit"></i></a>
                        <a href="<?php echo site_url('admin/users/'.$user->id.'/delete') ?>" class="btn btn-danger"><i class="fa fa-fw fa-times"></i></a>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
