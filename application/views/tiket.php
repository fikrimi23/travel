  <style>
    .panel-heading{
        text-align: center;
    }
    .panel-body img{
        width: 100%;
    }
    .sub-judul{
        text-align: center;
    }
    .fitur{
        font-family: tahoma;
        margin-left: 20px;
    }
    .fitur ol{
        border: 1px dashed black;
    }

  </style>

    <div class="container" style="margin-top: 40px">
        <div class="row">
            <?php foreach ($packages as $package): ?>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3><?php echo $package->tipe ?></h3></div>
                        <div class="panel-body">
                            <img src="<?php echo asset_url('gambar/'.$package->nama) ?>.png" alt="paket">
                            <div class="row">
                                <h4 class="sub-judul">Fasilitas</h4>
                                <div class="well">
                                    <ol>
                                    <?php foreach ($package->fasilitasShow as $show): ?>
                                        <li><?php echo $show ?></li>
                                    <?php endforeach ?>
                                    </ol>

                                    <center><button type="button" class="btn btn-info" data-toggle="modal" data-target="#<?php echo $package->tipe; ?>-lengkap">lebihlengkap</button></center>
                                </div>
                                <!--Modal-->
                                <div class="modal fade" id="<?php echo $package->tipe; ?>-lengkap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><center>Ket Lebih Lanjut</center></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="well">
                                                        <h5 class="sub-judul">Fasiltias</h5>
                                                    </div>
                                                    <div class="well">
                                                        <p class="fasilitas">
                                                            <ol>
                                                            <?php foreach ($package->fasilitas as $show): ?>
                                                                <li><?php echo $show ?></li>
                                                            <?php endforeach ?>
                                                            </ol>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="well">
                                                        <h5 class="sub-judul">Tidak termasuk</h5>
                                                    </div>
                                                    <div class="well">
                                                        <p class="fitur">
                                                            <ol>
                                                                <?php foreach ($package->fasilitasNone as $show): ?>
                                                                    <li><?php echo $show ?></li>
                                                                <?php endforeach ?>
                                                            </ol>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
<!--                                             <div class="well">
                                                <h3 class="sub-judul">Jadwal Perjalanan</h3>
                                                <div>
                                                    Bali -> Lombok -> Bali ->Pulang
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <!--Harga dan Beli-->
                                <center><p>Ayo Pesan Sekarang Juga disini juga dan jangan sampai kehabisan</p>
                                <a href="<?php echo site_url('pesan/'.$package->id) ?>"><button type="button" class="btn btn-success btn-lg"><i class="fa fa-fw fa-cart-arrow-down"></i><?php echo number_format($package->harga, 0, ",", ".") ?> IDR </button></a></center>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
