<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <h2>Hapus Data</h2>
            </div>
            <div class="alert-warning alert">
                <p>
                    <strong>Peringatan!</strong>
                    Apakah Anda yakin ingin menghapus data ini?
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <table class="table-bordered table">
                <div class="row">
                    <div class="form-inline">
                        <label class="label-control col-sm-3">ID</label>
                        <label class="label-control col-sm-1">:</label>
                        <p><?php echo $mhs['id']; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="form-inline">
                        <label class="label-control col-sm-3">Nama</label>
                        <label class="label-control col-sm-1">:</label>
                        <p><?php echo $mhs['name']; ?></p>
                    </div>    
                </div>
                <div class="row">
                    <div class="form-inline">
                        <label class="label-control col-sm-3">Email</label>
                        <label class="label-control col-sm-1">:</label>
                        <p><?php echo $mhs['email']; ?></p>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <form method="post" action="<?php echo site_url('delete/' . $mhs['id']); ?>">
                        <div class="col-sm-3">
                            <div class="form-inline">
                                <input type="hidden" name="id" value="<?php echo $mhs['id']; ?>">
                                <input class="btn-danger btn" name="confirm" value="Ya" type="submit">
                                <input class="btn-default btn" name="confirm" value="Tidak" type="submit">
                            </div>
                        </div>
                    </form>
                </div>
            </table>
        </div>
    </div>
</div>