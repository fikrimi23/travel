<div class="container">
    <div class="row">
        <div class="page-header">
            <h2>Edit Data</h2>
        </div>
        <div class="col-sm-6">
            <form method="post" action="<?php echo site_url('edit_proses/' . $mhs['id']); ?>">
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name" value="<?= $mhs['name']; ?>" placeholder="Masukkan nama Anda di sini...">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="text" name="email" value="<?= $mhs['email']; ?>" placeholder="Masukkan email Anda di sini...">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="password" name="password" value="<?= $mhs['password']; ?>" placeholder="Masukkan password Anda di sini...">
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="OK" class="btn btn-primary">
                    <button class="btn-danger btn" type="reset">Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>