<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <form action="<?php echo site_url('tiket') ?>" method="post">
                <div class="row text-center">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4>Destinasi</h4>
                            <br>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#domestik">
                                LOCAL
                            </button>
                            &nbsp;
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#internasional">
                                INTERNATIONAL
                            </button>
                        </div>
                        <br>
                        <div id="pilihanDestinasi">
                            <img hidden style="width:auto; height:250px;" id="pilihanDestinasiImage" src="" alt=""  data-value="bali">
                            <input type="hidden" value="" name="destinasi">
                        </div>
                        <br>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4>Tanggal</h4>
                            <br>
                            <input type="text" name="date" id="date">
                        </div>
                        <br>
                    </div>
                </div>
                <!-- <h4>Pilihan Destinasi</h4> -->
                <!-- <p>Internasional-Singapore</p> -->
                <center><input type="submit" value="ORDER NOW" class="btn btn-success btn-lg"></center>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="domestik" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><center>Pilih Destinasi Domestik</center></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/bali.png'); ?>" alt=""  class="destination" data-value="bali"></a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/bandung.png'); ?>" alt="" class="destination" data-value="bandung"></a>
                    </div>
                </div>

                <div class="row" style="margin-top: 30px">
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/jakarta.png'); ?>" alt=""  class="destination" data-value="jakarta"></a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/jogja.png'); ?>" alt=""  class="destination" data-value="jogja"></a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="internasional" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><center>Pilih Destinasi Internasional</center></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/italy.png'); ?>" alt="" class="destination" data-value="italy"></a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/london.png'); ?>" alt="" class="destination" data-value="london"></a>
                    </div>
                </div>

                <div class="row" style="margin-top: 30px">
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/malaysia.png'); ?>" alt="" class="destination" data-value="malaysia"></a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/newyork.png'); ?>" alt="" class="destination" data-value="newyork"></a>
                    </div>
                </div>

                <div class="row" style="margin-top: 30px">
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/singapore.png'); ?>" alt="" class="destination" data-value="singapore"></a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#"><img src="<?php echo asset_url('gambar/sydney.png'); ?>" alt="" class="destination" data-value="sydney"></a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#date').bootstrapMaterialDatePicker({time:false});
        $('.destination').click(function() {
            $('#domestik, #internasional').modal('hide');
            $('#pilihanDestinasiImage').attr('src', $(this).attr('src'));
            $('input[name=destinasi]').val($(this).attr('data-value'));
            $('#pilihanDestinasiImage').removeAttr('hidden');
            return false;
        });
    });
</script>
