  <style>
    #int{
      margin-right: 10px;
    }
    #dom{
      margin-right: 10px;
    }
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
      margin-top: 20px;
    }
    
	.carousel-inner img {
	  width: 100%; /* Set width to 100% */
	  margin: auto;
	  min-height:200px;
	  max-height: 400px;
	}

	/* Hide the carousel text when the screen is less than 600 pixels wide */
	@media (max-width: 600px) {
	.carousel-caption {
	  display: none; 
	}
	}

	.container-text-center img{
	width: 180px;
	height: 80px;
	}
	.icon{
	  width: 25px;
	  height: 25px;
	}
	.img-cmt{
	width: 50px;
	height: 50px;
	}
	.img-sh{
		width: 150px;
		height: 200px;
	}
  </style>
    <!--Gambar-->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
        <li data-target="#myCarousel" data-slide-to="6"></li>
        <li data-target="#myCarousel" data-slide-to="7"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="gbr-slide" src="http://tripbromo.com/wp-content/uploads/2016/09/Paket-Wisata-Ke-Bromo-Dari-Surabaya.jpg" alt="Image">
          <div class="carousel-caption">
            <h3>Bromo</h3>
            <p>Surga Wisata Di Kawasan Segetiga Wisata Jawa Timur</p>
          </div>      
        </div>
  
        <div class="item">
          <img class="gbr-slide" src="https://pix16.agoda.net/city/17193/17193-7x3.jpg" alt="Image">
          <div class="carousel-caption">
           <h3>Bali</h3>
           <p>Pulau Dengan 1001 Kekayaan Wisatanya</p>
          </div>      
        </div>
  
        <div class="item">
          <img class="gbr-slide" src="http://www.balikoala.com/wp-content/uploads/2016/03/lombok.jpg" alt="Image">
          <div class="carousel-caption">
            <h3>Lombok</h3>
            <p>Destinasi Wisata Baru yang Tidak Bakal Kamu Lupakan</p>
          </div>      
        </div>

        <div class="item">
          <img class="gbr-slide" src="http://raja-ampat.biz/wp-content/uploads/slide09.jpg" alt="Image">
          <div class="carousel-caption">
            <h3>Raja Ampat</h3>
            <p>Surga Indonesia Timur yang Wajib Kamu Kunjungi</p>
          </div>      
        </div>
    <div class="item">
          <img class="gbr-slide" src="http://www.universal-tourguide.com/wp-content/uploads/2016/09/discoverlondon.jpg" alt="Image">
          <div class="carousel-caption">
           <h3>London</h3>
           <p>Pulau Dengan 1001 Kekayaan Wisatanya</p>
          </div>      
        </div>
    <div class="item">
          <img class="gbr-slide" src="https://www.cornellclubnyc.com/Images/Library/slider-2.jpg" alt="Image">
          <div class="carousel-caption">
           <h3>New York</h3>
           <p>Pulau Dengan 1001 Kekayaan Wisatanya</p>
          </div>      
        </div>
    <div class="item">
          <img class="gbr-slide" src="http://www.pacificworld.com/webwp/wp-content/uploads/2016/03/italy.jpg" alt="Image">
          <div class="carousel-caption">
           <h3>Italy</h3>
           <p>Pulau Dengan 1001 Kekayaan Wisatanya</p>
          </div>      
        </div>    
    <div class="item">
          <img class="gbr-slide" src="http://www.amabeltravel.com/wp-content/uploads/2016/02/tugu-jogja.jpg" alt="Image">
          <div class="carousel-caption">
           <h3>Jogja</h3>
           <p>Pulau Dengan 1001 Kekayaan Wisatanya</p>
          </div>      
        </div>
    <div class="item">
          <img class="gbr-slide" src="https://pix6.agoda.net/city/8691/8691-7x3.jpg" alt="Image">
          <div class="carousel-caption">
           <h3>Jakarta</h3>
           <p>Pulau Dengan 1001 Kekayaan Wisatanya</p>
          </div>      
        </div>    
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>

    <div class="row" style="margin-top: 20px">
      <div class="col-sm-6 well">
        <center><h3>Beberapa Testimoni</h3></center>
        <table class="table table-hover">
          <tr>
            <td><img class="img-cmt" src="gambar/twitter.png" alt=""></td>
            <td><p><b>@rudi :</b></p><p>Pelayanan yang sangat memuaskan, terimakasi fun vacation semoga makin wah. Terimakasih</p></td>
          </tr>
          <tr>
            <td><img class="img-cmt" src="gambar/twitter.png" alt=""></td>
            <td><p><b>@johnnotchena :</b></p><p>Harga yang sangat terjangkau dengan kualitas TOP, saya sangat sarankan untuk berlanggan disini</p></td>
          </tr>
          <tr>
            <td><img class="img-cmt" src="gambar/twitter.png" alt=""></td>
            <td><p><b>@mawarBodas :</b></p><p>Awalnya sempat ragu, tapi berhubung beberapa rekomendasi dari teman. Dan yang paling suka paket wisata ke New York yang terjangkau</p></td>
          </tr>
        </table>
      </div>
      <div class="col-sm-6 well">
        <center><h3>Ingin Seperti Mereka?</h3></center>
        <table class="table table-hover">
          <tr>
            <td><img class="img-sh" src="gambar/bali_trip.jpg" alt=""></td>
            <td><img class="img-sh" src="gambar/singapore_trip.jpg" alt=""></td>
            <td><img class="img-sh" src="gambar/italy_trip.jpg" alt=""></td>
          </tr>
          <tr>
          </tr>
        </table>
      </div>        
    </div>
