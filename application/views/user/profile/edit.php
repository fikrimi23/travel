<style>
    #int{
      margin-right: 10px;
    }
    #dom{
      margin-right: 10px;
    }
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
      max-height: 400px;
    }

    /* Hide the carousel text when the screen is less than 600 pixels wide */
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; 
      }
    }

    .container-text-center img{
      width: 180px;
      height: 80px;
    }

    .img-circle{
      height: 300px;
      widows: 300px;
    }

	/* .crew-photo{
	height: 100px;
	widows: 80px;
	} */

	.panel-body{
	height: 150px;
	}
	.icon{
	width: 25px;
	height: 25px;
	}
	form div .row{
	margin-top: 20px;
	}
</style>
    <!--ISI-->
    <div class="well" style="">
    	<div class="row">
    		<div class="col-sm-offset-2 col-sm-8">
			<center><h3>Update Profil</h3></center>
			<form method="post" action="<?php echo site_url('user/profile/edit_process'); ?>">
				<table class="table table-hover">
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><input type="text" id='username' name='username' placeholder="<?php echo $user->username ?>"disabled></td>
					</tr>
					 <tr>
					   <td>Name</td>
					   <td>:</td>
					   <td><input type="text" id='nama' name='nama' placeholder="<?php echo $user->nama ?>"></td>
					 </tr>
					 <tr>
					   <td>Address</td>
					   <td>:</td>
					   <td><input type="text" id="alamat" name='alamat' placeholder="<?php echo $user->alamat ?>"></td>
					 </tr>
					 <tr>
					   <td>Phone Number</td>
					   <td>:</td>
					   <td><input type="text" id="telp" name='telp' placeholder="<?php echo $user->telp ?>"></td>
					 </tr>
					 <tr>
					   <td>Email</td>
					   <td>:</td>
					   <td><input type="text" id="email" name='email' placeholder="<?php echo $user->email ?>"></td>
					 </tr>
				</table>
				<center><input type="submit" class="btn btn-success" placeholder="Update"></center>
			</form>
			</div>
		</div>
	</div>