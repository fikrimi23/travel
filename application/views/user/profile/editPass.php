<style>
    #int{
      margin-right: 10px;
    }
    #dom{
      margin-right: 10px;
    }
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
      max-height: 400px;
    }

    /* Hide the carousel text when the screen is less than 600 pixels wide */
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; 
      }
    }

    .container-text-center img{
      width: 180px;
      height: 80px;
    }

    .img-circle{
      height: 300px;
      widows: 300px;
    }

	/* .crew-photo{
	height: 100px;
	widows: 80px;
	} */

	.panel-body{
	height: 150px;
	}
	.icon{
	width: 25px;
	height: 25px;
	}
	form div .row{
	margin-top: 20px;
	}
</style>
<div class="well" style="">
    <div class="row">
    	<div class="col-sm-offset-2 col-sm-8">
			<center><h3>Ganti Password</h3></center>
			<form method="post" action="<?php echo site_url('user/profile/editPass_process'); ?>">
				<div class="row" style="margin-top:10px">
					<div class="col-sm-4">Password Lama</div>
					<div class="col-sm-8"><input type="password" id="pwd-old"></div>
				</div>
				<div class="row" style="margin-top:10px">
				  <div class="col-sm-4">Password Baru</div>
				  <div class="col-sm-8"><input type="password" name="passnew" id="passnew"></div>
				</div>
				<div class="row" style="margin-top:10px">
				  <div class="col-sm-4">Input Ulang Password Baru</div>
				  <div class="col-sm-8"><input type="password" name="passcek" id="passcek"></div>
				</div>
				<div class="row" style="margin-top:10px">
				<center><input type="submit" class="btn btn-info btn-lg" value="Ubah Password"></center>
				</div>
            </form>
        </div>
    </div>
</div>