<div class="container">
<?php foreach ($transactions as $transaction): ?>
    <div class="panel panel-<?php echo $status[$transaction->status]['status_color'] ?>">
        <div class="panel-heading"></div>
        <div class="panel-body row">
            <div class="col-sm-6">
                <h3><?php echo $transaction->nama ?></h3>
                <h3><?php echo $transaction->departDate ?></h3>
                <h3><?php echo number_format($transaction->harga, 0, ",", ".") ?> IDR </h3>
            </div>
            <div class="col-sm-3">
                <h1><?php echo $transaction->tipe ?></h1>
            </div>
            <div class="col-sm-3">
                <h1><?php echo $status[$transaction->status]['status_name'] ?></h1>
                <?php if ($status[$transaction->status]['status_name'] == "PENDING"): ?>
                    <h1><a class="btn btn-default" href="<?php echo site_url('user/transaction/order') ?>" >Order</a><a class="btn btn-danger" href="<?php echo site_url('user/transaction/cancelOrder') ?>" >Batal</a></h1>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endforeach ?>
</div>
