<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-primary" style="margin-top: 15%;">
                <div class="panel-heading">
                    <h3>Login</h3>
                </div>
                <div class="panel-body">
                    <?php if (isset($_SESSION['message'])): ?>
                        <div class="alert-danger alert">
                            <?php echo $_SESSION['message']; ?>
                        </div>
                    <?php endif; ?>
                    <form method="post" action="<?php echo site_url('auth/login_auth'); ?>">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" placeholder="example@mail.com">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Masukkan password disini..">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" value="Login" class="btn btn-success">
                            <a class="btn btn-link" href="<?php echo site_url('auth/register'); ?>">Register</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>