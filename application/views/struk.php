  <style>
  	.panel-heading{
  		text-align: center;
  	}
  	.panel-body img{
  		width: 100%;
  	}
  	.sub-judul{
  		text-align: center;
  	}
  	.fitur{
  		font-family: tahoma;
  		margin-left: 20px;
  	}
  	.fitur ol{
  		border: 1px dashed black;
  	}
  	.img-des{
  		width: 100%;
  		height: 150px;
  	}

  </style>
	<div class="container" style="margin-top: 40px">
		<div class="row">
			<div class="col-sm-offset-3 col-sm-6 well">
				<center><div class="well">
					<h4>Tujuan : <?php echo $package->nama ?></h4>
					<p>Tanggal Pemesanan : <?php echo (new DateTime())->format('d F Y') ?></p>
				</div></center>
				<div class="row well">
					<div class="col-sm-4"><img class="img-des" src="<?php echo asset_url('gambar/').$package->nama ?>.png" alt=""></div>
					<div class="col-sm-8">
						<ul>
							<li><p>Paket : <?php echo $package->nama ?></p></li>
                            <?php if (isset($_SESSION['username'])): ?>
    							<li><p>Nama Pemesan : <?php echo $_SESSION['username'] ?></p></li>
                            <?php endif ?>
							<li><p>Tanggal Pemberangkatan: <?php echo (new DateTime($_SESSION['date']))->format('d F Y'); ?></p></li>
						</ul>
					</div>
				</div>
				<div class="row well">
					<table class="table table-hover">
						<tr>
							<td>Jumlah Pesanan</td>
							<td><p>: 1</p></td>
						</tr>
						<tr>
							<td>Harga Satuan</td>
							<td><p>: <?php echo number_format($package->harga, 0, ",", ".") ?> IDR </p></td>
						</tr>
						<tr>
							<td>Total</td>
							<td><p>: <?php echo number_format($package->harga, 0, ",", ".") ?> IDR </p></td>
						</tr>
					</table>
				</div>
                <?php if ($_SESSION['is_login'] == 1): ?>
                    <center><a class="btn btn-info" data-toggle="modal" href="<?php echo site_url('user/transaction/order') ?>">Pesan Sekarang</a></center>
                <?php else: ?>
                    <center><a class="btn btn-info" data-toggle="modal" data-target="#login">Pesan Sekarang</a></center>
                <?php endif ?>
	      	</div>
			</div>
		</div>
	</div>
