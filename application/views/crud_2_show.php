<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <h2>Home</h2>
            </div>
            <?php if (isset($this->session->message)): ?>
                <div class="alert-success alert">
                    <?= $this->session->message; ?>
                </div>
            <?php endif; ?>
            <p>
                Selamat datang, <?= $this->session->name;?>.
                    [<font color="red"><?= $this->session->role; ?></font>]. 
                    <a href="<?php echo site_url('logout'); ?>">Logout</a>
            </p>
            <table class="table-bordered table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <?php if ($this->session->role == 'admin'):?>
                            <th>Action</th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    if ($this->session->role == 'admin')
                    {
                        foreach ($mhs as $mahasiswa) { ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $mahasiswa['name']; ?></td>
                            <td><?= $mahasiswa['email']; ?></td>
                            <td>
                                <a class="btn btn-warning btn-sm" href="<?php echo site_url('edit/' . $mahasiswa['id']); ?>">Edit</a>
                                <a class="btn btn-danger btn-sm" href="<?php echo site_url('delete_confirm/' . $mahasiswa['id']); ?>">Delete</a>
                            </td>
                        </tr>
                        <?php $no++;
                        }
                    }
                    else {
                        foreach ($mhs as $mahasiswa) { ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $mahasiswa['name']; ?></td>
                            <td><?= $mahasiswa['email']; ?></td>
                        </tr>
                        <?php $no++;
                        }
                    } ?>              
                </tbody>
            </table>
        </div>
    </div>
</div>