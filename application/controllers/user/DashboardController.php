<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ( ! is_logged_in()) {
            redirect('/');
        }
    }

    public function index()
    {
        $this->load->view('user/dashboard/index');
    }
}
