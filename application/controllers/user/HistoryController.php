<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HistoryController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ( ! is_logged_in()) {
            redirect('/');
        }

        $this->status = [
            0 => ['status_name' => 'DONE', 'status_color' => 'success'],
            1 => ['status_name' => 'WAITING', 'status_color' => 'warning'],
            2 => ['status_name' => 'ORDERED', 'status_color' => 'info'],
            3 => ['status_name' => 'PENDING', 'status_color' => 'danger'],
        ];

    }

    public function index()
    {
        $this->load->model(['transaction_model', 'package_model']);
        $all_transactions = $this->transaction_model->get_all($_SESSION['username']);

        if ( ! empty($_SESSION['destinasi'])) {
            $pending_transaction = $this->package_model->get_with_dest_tipe($_SESSION['destinasi'], $_SESSION['tipe']);

            $pending_transaction[0]->departDate = $_SESSION['date'];
            $pending_transaction[0]->status = 3;

            // DEBUG

            $all_transactions = array_merge($pending_transaction, $all_transactions);
        }

        $data = [
            'transactions' => $all_transactions,
            'status' => $this->status
        ];

        $this->load->view('template/header');
        $this->load->view('user/history/index', $data);
        $this->load->view('template/footer');
    }

    public function edit()
    {
        $this->load->view('user/profile/edit');
    }

    public function update()
    {
    }
}
