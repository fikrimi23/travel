<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ( ! is_logged_in()) {
            redirect('/');
        }
		
		$this->load->model('user_model');
		$this->user=$this->user_model->get_one_by_username($_SESSION['username']);
    }

    public function index()
    {
		$data=[
			'user'=>$this->user
		];
		$this->load->view('template/header');
        $this->load->view('user/profile/profil',$data);
		$this->load->view('template/footer');
    }

    public function edit()
    {
		$data=[
			'user'=>$this->user
		];
        $this->load->view('template/header');
        $this->load->view('user/profile/edit',$data);
		$this->load->view('template/footer');
    }
	
	public function do_edit()
    {
		$data=[
			'user'=>$this->user
		];
		$id=$this->user->id;
		$new_data=array(
		'nama'=> $this->input->post('nama'),
		'email'=> $this->input->post('email'),
		'alamat'=> $this->input->post('alamat'),
		'telp'=> $this->input->post('telp')
		);
		$this->load->model('user_model');
		$this->user_model->updateprofile($id,$new_data);
		
		redirect('user/profile');
    }
	
    public function editPass()
    {
		$data=[
			'user'=>$this->user
		];
		$this->load->view('template/header');
        $this->load->view('user/profile/editPass',$data);
		$this->load->view('template/footer');
    }
	
	public function do_editPass()
    {$data=[
			'user'=>$this->user
		];
		$id=$this->user->id;
		$new_data=array(
		'password'=>password_hash($this->input->post('passnew'), PASSWORD_DEFAULT)
		);
		$this->load->model('user_model');
		$this->user_model->updateprofilepass($id,$new_data);
		redirect('user/profile');
    }
}
