<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        if ( ! is_logged_in() || ! $this->user_model->is_admin($_SESSION['username'])) {
            // redirect('/');
        }
    }

    public function index()
    {
        $users = $this->user_model->get_all();

        $data = [
            'users' => $users
        ];

        $this->load->view('template/header');
        $this->load->view("admin/index", $data);
        $this->load->view('template/footer');
    }

    public function edit()
    {
        $this->load->view('user/admin/edit');
    }

    public function update()
    {
    }

    public function destroy($id)
    {
        $user = $this->user_model->get_one_by_username($_SESSION['username']);

        if ($user->id != $id) {
            $this->user_model->destroy($id);
        }
        redirect('admin/users');
    }
}
