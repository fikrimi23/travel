<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ( ! is_logged_in()) {
            redirect('/');
        }

        $this->status = [
            0 => ['status_name' => 'DONE', 'status_color' => 'success'],
            1 => ['status_name' => 'WAITING', 'status_color' => 'warning'],
            2 => ['status_name' => 'ORDERED', 'status_color' => 'info'],
            3 => ['status_name' => 'PENDING', 'status_color' => 'danger'],
        ];
    }

    public function index()
    {
        $this->load->view('user/profile/index');
    }

    public function edit()
    {
        $this->load->view('user/profile/edit');
    }

    public function update()
    {
    }

    public function order()
    {
        $this->load->model(['transaction_model', 'package_model', 'user_model']);

        $package = $this->package_model->get_with_dest_tipe($this->session->destinasi, $this->session->tipe);

        $data = [
            'user_id' => $this->user_model->get_one_by_username($this->session->username)->id,
            'package_id' => $package[0]->id,
            'faktur' => random_int(0, 99999999),
            'status' => 2,
            'departDate' => $this->session->date,
        ];

        $this->transaction_model->insert($data);

        $this->session->destinasi = '';
        $this->session->date = '';
        $this->session->paket = '';

        redirect('user/history');
    }

    public function cancelOrder()
    {
        $this->session->destinasi = '';
        $this->session->date = '';
        $this->session->paket = '';
        redirect('user/history');
    }
}
