<?php
/**
*
*/

class HomeController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
    * Menampilkan sebuah halaman
    * @param string $page
    * @param array $data (default value = null)
    */
    function view($page, $data = null)
    {
        $this->load->view('templates/header', $data);
        $this->load->view($page, $data);
        $this->load->view('templates/footer');
    }

    /**
    * Menampilkan session message
    * @param string $message
    */
    function set_session_message($message)
    {
        $this->session->message = $message;
        $this->session->mark_as_flash('message');
    }

    /**
    * Mengecek jenis request yang diterima
    */
    function check_server_method()
    {
        if($_SERVER['REQUEST_METHOD'] !== "POST")
            exit("Direct access is not allowed!");
    }

    /**
    * Menampilkan halaman utama
    */
    function index()
    {
        $this->session->is_login or redirect('login');

        $data['title'] = "Home";
        $data['mhs'] = $this->user_model->get("tblmhs");
        $this->view("crud_2_show", $data);
    }

    /**
    * Menampilkan halaman edit
    * @param int $id
    */
    function edit($id)
    {
        $this->session->is_login or redirect('login');

        $data['mhs'] = $this->user_model->get("tblmhs", $id);
        $data['title'] = 'Edit';
        $this->view('crud_2_edit', $data);
    }

    /**
    * Mmemproses perubahan data
    * @param int $id
    */
    function edit_proses($id)
    {
        $this->check_server_method();

        $data_edit = $this->input->post();
        unset($data_edit['submit']);
        // var_dump($data_edit);
        $query = $this->user_model->update("tblmhs", $id, $data_edit);
        if ($query)
        {
            $this->set_session_message("Berhasil meng-update data!");
            redirect('home');
        }
        else
        {
            echo "An error occured.";
        }
    }

    /**
    * Menampilkan halaman konfirmasi hapus data
    * @param int $id
    */
    function delete_confirm($id)
    {
        $this->session->is_login or redirect('login');

        $data['title'] = 'Hapus Data';
        $data['mhs'] = $this->user_model->get("tblmhs", $id);
        $this->view("crud_2_delete", $data);
    }

    /**
    * Memroses data yang dihapus
    * @param int $id
    */
    function delete($id)
    {
        $this->check_server_method();

        $query = $this->user_model->delete("tblmhs", $id);
        if ($query)
        {
            $this->set_session_message('Data berhasil dihapus!');
            redirect('home');
        }
        else
        {
            echo "An error occured.";
        }
    }
}
