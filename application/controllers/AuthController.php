<?php
class AuthController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
	//view login
    function login()
    {
        $data['title'] = "Login";
		$this->load->view('template/header', $data);
        $this->load->view('auth/login');
        $this->load->view('template/footer');
    }
	//algoritma login
    function do_login()
    {
        if($_SERVER['REQUEST_METHOD'] !== "POST")
            exit('Direct access is not allowed!');

		$this->load->model('user_model');
        if($user=$this->user_model->login()){
			$this->session->username = $user->username;
            $this->session->is_login = true;
            redirect('home');
        }
        else
        {
            echo "Error mendaftarkan user!";
        }
    }

    //view register
    function register()
    {
        $data['title'] = "Register";
        $this->load->view('template/header', $data);
        $this->load->view('auth/register');
        $this->load->view('template/footer');
    }

    //algoritma register
    function do_register()
    {
        if($_SERVER['REQUEST_METHOD'] !== "POST")
            exit('Direct access is not allowed!');

		$this->load->model('user_model');
        if($this->user_model->insert())
        {
			//show register berhasil
            redirect('home');
        }
        else
        {
            echo "Error mendaftarkan user!";
        }
    }

    //algoritma logout
    function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
