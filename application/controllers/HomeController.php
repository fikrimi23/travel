<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        if ( ! isset($_SESSION['destinasi'])) {
            $this->session->destinasi = '';
        }

        if ( ! isset($_SESSION['date'])) {
            $this->session->date = '';
        }

        if ( ! isset($_SESSION['paket'])) {
            $this->session->paket = '';
        }

        if ( ! isset($_SESSION['is_login'])) {
            $this->session->is_login = '';
        }

        if ( ! isset($_SESSION['is_admin'])) {
            $this->session->is_admin = '';
        } elseif ( isset($_SESSION['is_login']) && ! empty($_SESSION['username'])) {
            $this->session->is_admin = $this->user_model->is_admin($_SESSION['username']);
        }
    }

    /**
     * View untuk menampilkan halaman index
     * @return void
     */
    public function index()
    {
        if (empty($_SESSION['username'])) {
            $this->session->destinasi = '';
            $this->session->date = '';
            $this->session->paket = '';
        }
        $this->load->view('template/header');
        $this->load->view("index");
        $this->load->view('template/footer');
    }

	public function faq()
    {
        $this->load->view('template/header');
        $this->load->view('faq');
        $this->load->view('template/footer');
    }

    public function gallery()
    {
        $this->load->view('template/header');
        $this->load->view('gallery');
        $this->load->view('template/footer');
    }

    /**
     * View untuk menampilkan halaman about
     * @return void
     */
    public function about()
    {
        $this->load->view('template/header');
        $this->load->view('about');
        $this->load->view('template/footer');
    }

    /**
     * View untuk menampilkan halaman pemilihan tipe tiket
     * @return void
     */
    public function tiket()
    {
        // Set session
        $this->session->destinasi = $this->input->post('destinasi');
        $this->session->date = $this->input->post('date');

        if (empty($_SESSION['destinasi']) || empty($_SESSION['date'])) {
            redirect('/');
        }

        // Ambil seluruh paket yang ber destinasi input
        $this->load->model('package_model');
        $packages = $this->package_model->get_with_dest($_SESSION['destinasi']);
        if (count($packages) == 0 || new DateTime() > new DateTime($_SESSION['date'])) {
            redirect('/');
        }
        $data = [
            'packages' => $this->filterPackages($packages),
        ];

        // load view
        $this->load->view('template/header2');
        $this->load->view('tiket', $data);
        $this->load->view('template/footer');
    }

    /**
     * View untuk menampilkan halaman struk
     * @param  int $paket_id paket no
     * @return void
     */
    public function pesan($paket_id)
    {
        // Ambil 1 paket dengan paket 1 tersebut
        $this->load->model('package_model');
        $package = $this->package_model->get($paket_id);

        if (strtolower($package->nama) != strtolower($this->session->destinasi)) {
            redirect('/');
        }

        // Masukan paket tipe ke dalam session
        $this->session->tipe = $package->tipe;

        $data = [
            'package' => $this->filterPackages($package),
        ];

        // load view
        $this->load->view('template/header2');
        $this->load->view('struk', $data);
        $this->load->view('template/footer');
    }

    /**
     * Filter untuk melakukan exploding terhadap string Fitur
     * @param  Array $packages 1 paket atau banyak
     * @return Array
     */
    private function filterPackages($packages)
    {
        if (is_array($packages)) {
            foreach ($packages as $package) {
                // Explode sehingga menjadi array
                $tmpFasilitas = explode(";", $package->fasilitas);
                if (count($tmpFasilitas) > 5) {
                    // jika lebih dari 5 maka tampilkan hanya 5 pertama
                    for ($i = 0; $i < 5; $i++) {
                        $package->fasilitasShow[$i] = $tmpFasilitas[$i];
                    }
                } else {
                    // Jika tidak tampilkan semuanya
                    $package->fasilitasShow = $tmpFasilitas;
                }
                $package->fasilitas = $tmpFasilitas;
                $package->fasilitasNone = explode(";", $package->fasilitasNone);
            }
        } else {
            $tmpFasilitas = explode(";", $packages->fasilitas);
            if (count($tmpFasilitas) > 5) {
                for ($i = 0; $i < 5; $i++) {
                    $packages->fasilitasShow[$i] = $tmpFasilitas[$i];
                }
            } else {
                $packages->fasilitasShow = $tmpFasilitas;
            }
            $packages->fasilitas = $tmpFasilitas;
            $packages->fasilitasNone = explode(";", $packages->fasilitasNone);
        }

        return $packages;
    }
}
