<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
    Function Name : asset_url
    Description   : with base_url that codeigniter already have, this make
                                    child function for asset
    Parameters    :
        - $url [String] = URL to File
 */

if ( ! function_exists('asset_url')) {
    function asset_url($url = null)
    {
        if ($url == null) {
            return base_url().'assets/';
        } else {
            return base_url().'assets/'.$url;
        }
    }
}

if ( ! function_exists('isset_val')) {
    function isset_val($var, $properties = null)
    {
        if (isset($var) && $var !== false && $var !== null) {
            if ($properties === null) {
                return $var;
            } elseif (is_array($var)) {
                if (isset($var[$properties])) {
                    return $var[$properties];
                } else {
                    return false;
                }
            } elseif (property_exists($var, $properties)) {
                return $var->$properties;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

if ( ! function_exists('prep_input')) {
    /**
     * Prepare input to be passed to Model, the form 'name' must be same as param
     *
     * @param string dynamic Catch dynamic parameter of form 'name', if array, set
     * it as the 'dynamic parameter'
     * @return array populated input data
     */
    function prep_input()
    {
        $CI =& get_instance();
        if (func_num_args() >= 1) {
            $input_set = func_get_args();

            if (is_array($input_set[0])) {
                $input_set = $input_set[0];
            }

            foreach ($input_set as $input) {
                $post = $CI->input->post($input);
                if (isset($post)) {
                    $passed_data[$input] = $CI->input->post($input);
                }
            }

            return $passed_data;
        }
    }
}

if ( ! function_exists('prep_input_obj')) {
    /**
     * Same as prep_input, return object
     *
     * @param string dynamic Catch dynamic parameter
     * @return object populated input data
     */
    function prep_input_obj()
    {
        $CI =& get_instance();
        if (func_num_args() >= 1) {
            $input_set = func_get_args();

            if (is_array($input_set[0])) {
                $input_set = $input_set[0];
            }

            $passed_data = new stdClass();
            foreach ($input_set as $input) {
                if ($CI->input->post($input)) {
                    $passed_data->$input = $CI->input->post($input);
                }
            }

            return $passed_data;
        }
    }
}

function is_logged_in()
{
    // return true;
    if (isset($_SESSION['username']) && ( ! empty($_SESSION['username']))) {
        return true;
    } else {
        return false;
    }
}
