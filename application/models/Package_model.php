<?php
class Package_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Ambil satu buah paket dengan id tertentu
     * @param  int $id id dari pake
     * @return Object
     */
    public function get($id)
    {
        return $this->db->from('packages')->join('destinations', 'destinations.id = packages.destination_id')->where('packages.id', $id)->get()->row();
    }

    /**
     * Ambil banyak paket dengan pilihan destinasi tertentu
     * @param  string $location pilihan destinasi
     * @return void
     */
    public function get_with_dest($location)
    {
        $loct = $this->db->from('destinations')->join('packages', 'destinations.id = packages.destination_id')->like('destinations.nama', $location)->get()->result();
        return $loct;
    }

    public function get_with_dest_tipe($location, $tipe)
    {
        $loct = $this->db->from('destinations')->join('packages', 'destinations.id = packages.destination_id')->like('destinations.nama', $location)->like('tipe', $tipe)->get()->result();
        return $loct;
    }
}
