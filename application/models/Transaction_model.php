<?php
class Transaction_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all($username)
    {
        return $this->db->from('transactions')->join('users', 'users.id = transactions.user_id')->join('packages', 'packages.id = transactions.package_id')->join('destinations', 'destinations.id = packages.destination_id')->where('users.username', $username)->get()->result();
    }

    public function insert($data)
    {
        return $this->db->insert('transactions', $data);
    }

}
