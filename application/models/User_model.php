<?php
/**
*
*/
class User_model extends CI_Model
{
    /**
    * Mengambil user berdasarkan email dan password
    * @param string $email
    * @param string $password
    * @return object
    */
	function login()
    {
		$username= $this->input->post('username');
		$password= $this->input->post('password');
		$user = $this->db->from('users')->where('username',$username)->get();
        if($user->num_rows() > 0){
			if(password_verify($password,$user->row()->password)){
				return $user->row();
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
    }

    /**
    * Menambahkan data baru ke database
    * @param string table
    * @param array $data
    * @return boolean
    */
    function insert()
    {
		$user_data=array(
		'email'=> $this->input->post('email'),
		'username'=> $this->input->post('username'),
		'password'=> password_hash($this->input->post('password'), PASSWORD_DEFAULT)
		);
		if(!$this->db->insert('users',$user_data))
			return false;

		$insert_id=$this->db->insert_id();

		$profile_data=array(
		'user_id'=> $insert_id,
		'nama'=> $this->input->post('name'),
		'alamat'=> $this->input->post('alamat'),
		'telp'=> $this->input->post('telp')
		);
		if(!$this->db->insert('profiles',$profile_data))
			return false;

		$group_data=array(
		'user_id'=> $insert_id,
		'group_id'=> 2
		);

		$this->db->insert('group_user',$group_data);

		return true;
    }

    /**
    * Mengambil user dari database berdasarkan id (opsional, bisa juga tidak)
    * @param string $table
    * @param int id (optional)
    * @return array
    */
    function get($table, $id = null)
    {
        if ($id !== null) {
            return $this->db->where('id', $id)->get($table)->row_array();
        }
        return $this->db->get($table)->result_array();
    }

    /**
    * Memproses request untuk meng-update data
    * @param string $table
    * @param int $id
    * @param array $data
    * @return boolean
    */
    function update($table, $id, $data)
    {
        return $this->db->where('id', $id)->set(
            array(
                'nama' => $data['nama'],
                'email' => $data['email'],
                'password' => $data['password']
            )
        )->update($table);
    }
	
	function updateprofile($id, $data)
    {
        $this->db->where('id', $id)->set(
            array(
                'nama' => $data['nama'],
                'telp' => $data['telp'],
                'alamat' => $data['alamat']
            )
        )->update('profiles');
		$this->db->where('id', $id)->set(
            array(
                'email' => $data['email'],
            )
        )->update('users');
		
		
		return true;
    }
	
	function updateprofilepass($id, $data)
    {
		$this->db->where('id', $id)->set(
            array(
                'password' => $data['password'],
            )
        )->update('users');
		
		
		return true;
    }
    /**
    * Menghapus data
    * @param string $table
    * @param int $id
    * @return boolean
    */
    function delete($table, $id)
    {
        return $this->db->from('users')->where('id', $id)->delete($table);
    }

    public function is_admin($username)
    {
        $query = $this->db->from('users')->join('group_user', 'group_user.user_id = users.id')->where('username', $username)->get()->result();
        $groups = [];
        foreach ($query as $object) {
            $groups[] = $object->group_id;
        }
        if (array_search('1', $groups)) {
            return true;
        } else {
            return false;
        }
    }

    public function get_all()
    {
        return $this->db->from('users')->join('profiles', 'profiles.user_id = users.id')->get()->result();
    }

    public function get_one($id)
    {
        return $this->db->from('users')->join('profiles', 'profiles.user_id = users.id')->where('users.id', $id)->get()->row();
    }

    public function get_one_by_username($username)
    {
        return $this->db->from('users')->join('profiles', 'profiles.user_id = users.id')->where('users.username', $username)->get()->row();
    }

    public function destroy($id)
    {
        $this->db->from('users')->where('id', $id)->delete();
    }

}
